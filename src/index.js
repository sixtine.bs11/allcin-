import './style.scss';
import 'bootstrap';


// améliorer la requete pepole
// mettre un message si aucun film/serie/actor correspond

$(function () {

    let $search = $('#search'),
        $film = $('#film'),
        $searchForm = $('#searchForm'),
        $tvShow = $('#tvShow'),
        $people = $('#people'),
        $anyMovies = $('#anyMovies'),
        $anyShow = $('#anyShow'),
        $anyPeople = $('#anyPeople'),
        $navMovies = $('#navMovies'),
        $navTvshow = $('#navTvshow'),
        $navPeople = $('#navPeople'),
        $popFilms = $('#popFilms'),
        $popSeries = $('#popSeries'),
        $popPeoples = $('#popPeoples'),
        $menu = $('#menu'),
        $namePeople = $('.namePeople'),
        $readMore = $('#readMore')
        ;

    // ajoute un evenement submit au formulaire avec une fonction anonyme
    $searchForm.on('submit', function (event) {
        // lui enlever l'évenement par défaut
        event.preventDefault();
        // récupérer la valeur dans l'input
        let searchFilm = $search.val();
        // vider le html 
        $film.html('');
        $tvShow.html('');
        $people.html('');

        //cacher populaireHTML
        $popFilms.hide();
        $popSeries.hide();
        $popPeoples.hide();
        $menu.show();

        // requète ajax films
        $.get(`
        https://api.themoviedb.org/3/search/movie?api_key=4e58262aefaf5f506a0beb7376366181&language=en-US&query=${searchFilm}&page=1&include_adult=false
        `, function (response) {
                for (const movie of response.results) {
                    // créer nos variables
                    let title = movie.title,
                        release_date = movie.release_date,
                        overview = movie.overview,
                        poster_path = movie.poster_path;
                    // créer une template avec l'image qui va bien
                    let template = `
                                    <div class ="containerSearch">
                                        <h2>${title}</h2>
                                        <div class ="containerAffichage">
                                            <div>
                                                <img src ="https://image.tmdb.org/t/p/w300${poster_path}" alt"cinema poster>
                                            </div>
                                        <div class ="textImg">
                                            <p>Release date : ${release_date}</p>
                                            <p>synopsis of the film : <br> ${overview}</p>
                                        </div>
                                        </div>
                                    </div>`;
                    // ajouter la templete dans la partie htmp voulu
                    $film.append(template);
                }
            });

        // requète ajax series
        $.get(`
        https://api.themoviedb.org/3/search/tv?api_key=4e58262aefaf5f506a0beb7376366181&language=en-US&query=${searchFilm}&page=1`,
            function (response) {
                for (const series of response.results) {
                    let name = series.original_name,
                        poster = series.poster_path,
                        first_date = series.first_air_date,
                        overview = series.overview;

                    let template = `
                                <div class ="containerSearch">
                                    <h2>${name}</h2>
                                    <div class ="containerAffichage">
                                        <div>
                                            <img src ="https://image.tmdb.org/t/p/w300${poster}" alt"cinema poster>
                                        </div>
                                    <div class ="textImg">
                                        <p>Release date : ${first_date}</p>
                                        <p>synopsis of the film : <br> ${overview}</p>
                                    </div>
                                    </div>
                                </div>`;
                    $tvShow.append(template);
                }

            });

        // requète ajax people
        $.get(`
        https://api.themoviedb.org/3/search/person?api_key=4e58262aefaf5f506a0beb7376366181&language=en-US&query=${searchFilm}&page=1&include_adult=false`,
            function (response) {
                for (const actor of response.results) {
                    let name = actor.name,
                        profile = actor.profile_path,
                        backdrop = actor.backdrop_path,
                        id = actor.id;

                    let template =
                        `<div  class="containerAffichagePeople col-6" >
                            <img src = "https://image.tmdb.org/t/p/w154/${profile}">
                            <p class = "namePeople">${name}</p>    
                        </div>`;
                    $people.append(template);

                }
            })
    });

    // filtre : evenement click + display none
    function hideBlock() {
        $anyMovies.on('click', function () {
            console.log($tvShow);
            // appel objet avec 1 élément
            $film[0].style.display = 'block';
            $tvShow[0].style.display = 'none';
            $people[0].style.display = 'none';
        });
        $anyShow.on('click', function () {
            $tvShow[0].style.display = 'block';
            $film[0].style.display = 'none';
            $people[0].style.display = 'none';
        });
        $anyPeople.on('click', function () {
            $people[0].style.display = 'block';
            $tvShow[0].style.display = 'none';
            $film[0].style.display = 'none';
        });
    }

    hideBlock();

    // afficher tout les films/movie populaires avec la navbar movies
    // requète ajax films populaires
    $navMovies.on('click', function () {
        // vider le html 
        $film.html('');
        $tvShow.html('');
        $people.html('');
        //afficher/cacher le display
        $popFilms.show();
        $popSeries.hide();
        $popPeoples.hide();
        $menu.hide();
        $.get(`
     https://api.themoviedb.org/3/movie/popular?api_key=4e58262aefaf5f506a0beb7376366181&language=en-US&page=1
     `, function (response) {
                // Faire réapparaitre le block film
                $film.show();
                for (const movie of response.results) {
                    // créer nos variables
                    let title = movie.title,
                        id = movie.id,
                        release_date = movie.release_date,
                        overview = movie.overview,
                        poster_path = movie.poster_path;

                    // créer une template avec l'image qui va bien
                    let template = `
                                    <div class ="containerSearch">
                                        <h2>${title}</h2>
                                        <div class ="containerAffichage">
                                            <div>
                                                <img src ="https://image.tmdb.org/t/p/w300${poster_path}" alt"cinema poster>
                                            </div>
                                        <div class ="textImg">
                                            <p>Release date : ${release_date}</p>
                                            <p>synopsis of the film : <br> ${overview}</p>
                                        </div>
                                        </div>
                                    </div>`

                    // ajouter la template dans la partie html voulu
                    $film.append(template);
                }
            })
    });
    // Fin requète film populaire
    //
    // requète ajax series poulaires
    $navTvshow.on('click', function () {
        // vider le html 
        $film.html('');
        $tvShow.html('');
        $people.html('');

        $popSeries.show();
        $popFilms.hide();
        $popPeoples.hide();
        $menu.hide();
        $.get(`
        https://api.themoviedb.org/3/tv/popular?api_key=4e58262aefaf5f506a0beb7376366181&language=en-US&page=1`
            , function (response) {
                $tvShow.show();
                for (const series of response.results) {
                    let name = series.original_name,
                        poster = series.poster_path,
                        first_date = series.first_air_date,
                        overview = series.overview;

                    let template = `
                                    <div class ="containerSearch">
                                        <h2>${name}</h2>
                                            <div class ="containerAffichage">
                                                <div>
                                                    <img src ="https://image.tmdb.org/t/p/w300${poster}" alt"cinema poster>
                                                </div>
                                                <div class ="textImg">
                                                <p>Release date : ${first_date}</p>
                                                <p>synopsis of the film : <br> ${overview}</p>
                                                </div>
                                            </div>
                                    </div>`;

                    $tvShow.append(template);
                }

            })
    });
    // Fin requète series poulaires
    //
    // requète ajax people populaires
    $navPeople.on('click', function () {
        // vider le html 
        $film.html('');
        $tvShow.html('');
        $people.html('');

        $popPeoples.show();
        $popFilms.hide();
        $popSeries.hide();
        $menu.hide();
        $.get(`
    https://api.themoviedb.org/3/person/popular?api_key=4e58262aefaf5f506a0beb7376366181&language=en-US&page=1`
            , function (response) {
                $people.show();
                for (const actor of response.results) {
                    let name = actor.name,
                        profile = actor.profile_path,
                        //backdrop = actor.backdrop_path,
                        //titleFilm = actor.original_title,
                        id = actor.id
                        ;

                    let template =
                        `<div  class="containerAffichagePeople col-6 ">
                                    <img src = "https://image.tmdb.org/t/p/w154/${profile}">
                                    <p class = "namePeople" id="${id}" >${name}</p>                            
                            </div>`;
                    $people.append(template);

                    $namePeople = $('.namePeople');
                    let $id = $(`#${id}`);

// faire une croix
                    $id.on('click', function () {
                        // on clear la div à chaque nouveau click
                        $readMore.html('');
                        $readMore.show();
                            $.get(`https://api.themoviedb.org/3/person/${id}?api_key=4e58262aefaf5f506a0beb7376366181&language=en-US`
                                , function (response) {
                                    // pas besoin d'une boucle forof pour une seule personne
                                    let birthday = response.birthday,
                                        name = response.name,
                                        biography = response.biography,
                                        place_of_birth = response.place_of_birth
                                        ;
                                    let tpl =
                                        `
                                                                <p class="info1">${name} birth ${birthday} from place of birth : ${place_of_birth}</p>
                                                                <p class ="info2"> Biography : ${biography}</p> 
                                                                <button type="button" class="closeInfo">x</button>
                                                            `;

                                    $readMore.append(tpl);
                                    let $closeInfo = $('.closeInfo');
                                    $closeInfo.on('click', function(){
                                        $readMore.hide();
                                    })
                                }
                            )
                        } 
                )
                    // fin du click
                }
            })
            // fin de la requète poppeople
    })
});
